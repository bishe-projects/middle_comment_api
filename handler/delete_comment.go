package handler

import (
	"context"
	"github.com/gin-gonic/gin"
	"gitlab.com/bishe-projects/middle_comment_api/client"
	"gitlab.com/bishe-projects/middle_comment_api/dto"
	"net/http"
)

func DeleteComment(c *gin.Context) {
	var deleteComment dto.DeleteComment
	if err := c.BindJSON(&deleteComment); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	ctxVal, _ := c.Get("context")
	ctx := ctxVal.(context.Context)
	resp, err := client.CommentClinet.DeleteComment(ctx, deleteComment.ConvertToReq())
	if err != nil {
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}
	c.JSON(http.StatusOK, resp)
}

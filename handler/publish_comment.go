package handler

import (
	"context"
	"github.com/gin-gonic/gin"
	"gitlab.com/bishe-projects/middle_comment_api/client"
	"gitlab.com/bishe-projects/middle_comment_api/dto"
	"net/http"
)

func PublishComment(c *gin.Context) {
	var publishComment dto.PublishComment
	if err := c.BindJSON(&publishComment); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	ctxVal, _ := c.Get("context")
	ctx := ctxVal.(context.Context)
	resp, err := client.CommentClinet.PublishComment(ctx, publishComment.ConvertToReq())
	if err != nil {
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}
	c.JSON(http.StatusOK, resp)
}

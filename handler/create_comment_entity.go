package handler

import (
	"context"
	"github.com/gin-gonic/gin"
	"gitlab.com/bishe-projects/middle_comment_api/client"
	"gitlab.com/bishe-projects/middle_comment_api/dto"
	"net/http"
)

func CreateCommentEntity(c *gin.Context) {
	var createCommentEntity dto.CreateCommentEntity
	if err := c.BindJSON(&createCommentEntity); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	ctxVal, _ := c.Get("context")
	ctx := ctxVal.(context.Context)
	resp, err := client.CommentClinet.CreateCommentEntity(ctx, createCommentEntity.ConvertToReq())
	if err != nil {
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}
	c.JSON(http.StatusOK, resp)
}

package handler

import (
	"context"
	"github.com/gin-gonic/gin"
	"gitlab.com/bishe-projects/middle_comment_api/client"
	"gitlab.com/bishe-projects/middle_comment_api/dto"
	"net/http"
)

func GetCommentByID(c *gin.Context) {
	var getCommentByID dto.GetCommentByID
	if err := c.BindJSON(&getCommentByID); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	ctxVal, _ := c.Get("context")
	ctx := ctxVal.(context.Context)
	resp, err := client.CommentClinet.GetCommentById(ctx, getCommentByID.ConvertToReq())
	if err != nil {
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}
	c.JSON(http.StatusOK, resp)
}

package handler

import (
	"context"
	"github.com/gin-gonic/gin"
	"gitlab.com/bishe-projects/middle_comment_api/client"
	"gitlab.com/bishe-projects/middle_comment_api/dto"
	"net/http"
)

func UserCommentList(c *gin.Context) {
	var userCommentList dto.UserCommentList
	if err := c.BindJSON(&userCommentList); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	ctxVal, _ := c.Get("context")
	ctx := ctxVal.(context.Context)
	resp, err := client.CommentClinet.UserCommentList(ctx, userCommentList.ConvertToReq())
	if err != nil {
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}
	c.JSON(http.StatusOK, resp)
}

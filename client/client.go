package client

import (
	"github.com/cloudwego/kitex/client"
	"github.com/cloudwego/kitex/transport"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/comment/commentservice"
)

var (
	CommentClinet = commentservice.MustNewClient("comment_service", client.WithHostPorts("127.0.0.1:8887"), client.WithTransportProtocol(transport.TTHeaderFramed))
)

package main

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/bishe-projects/common_utils/middleware"
	"gitlab.com/bishe-projects/middle_comment_api/handler"
)

func main() {
	r := gin.Default()
	r.Use(middleware.Cors(), middleware.VerifyTokenMiddleware())
	commentEntity := r.Group("/commentEntity")
	{
		commentEntity.POST("/create", handler.CreateCommentEntity)
		commentEntity.POST("/all", handler.AllCommentEntityList)
	}
	comment := r.Group("/comment")
	{
		comment.POST("/publish", handler.PublishComment)
		comment.POST("/delete", handler.DeleteComment)
		comment.POST("/getById", handler.GetCommentByID)
		comment.POST("/entityCommentList", handler.EntityCommentList)
		comment.POST("/userCommentList", handler.UserCommentList)
	}
	r.Run("0.0.0.0:8084")
}

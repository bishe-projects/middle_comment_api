package dto

import "gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/comment"

type EntityCommentList struct {
	BizId        int64  `json:"biz_id" binding:"required"`
	EntityTypeId int64  `json:"entity_type_id" binding:"required"`
	EntityId     int64  `json:"entity_id" binding:"required"`
	ParentId     int64  `json:"parent_id"`
	PageNum      *int64 `json:"page_num,omitempty"`
	PageSize     *int64 `json:"page_size,omitempty"`
}

func (d *EntityCommentList) ConvertToReq() *comment.EntityCommentListReq {
	return &comment.EntityCommentListReq{
		BizId:        d.BizId,
		EntityTypeId: d.EntityTypeId,
		EntityId:     d.EntityId,
		ParentId:     d.ParentId,
		PageNum:      d.PageNum,
		PageSize:     d.PageSize,
	}
}

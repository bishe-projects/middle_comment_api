package dto

import (
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/comment"
)

type AllCommentEntityList struct {
	BizId *int64 `json:"biz_id"`
}

func (d *AllCommentEntityList) ConvertToReq() *comment.AllCommentEntityListReq {
	return &comment.AllCommentEntityListReq{
		BizId: d.BizId,
	}
}

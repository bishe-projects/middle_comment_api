package dto

import (
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/comment"
)

type GetCommentByID struct {
	Id    int64 `json:"id" binding:"required"`
	BizId int64 `json:"biz_id" binding:"required"`
}

func (d *GetCommentByID) ConvertToReq() *comment.GetCommentByIdReq {
	return &comment.GetCommentByIdReq{
		Id:    d.Id,
		BizId: d.BizId,
	}
}

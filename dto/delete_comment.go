package dto

import "gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/comment"

type DeleteComment struct {
	Id    int64 `json:"id" binding:"required"`
	BizId int64 `json:"biz_id" binding:"required"`
}

func (d *DeleteComment) ConvertToReq() *comment.DeleteCommentReq {
	return &comment.DeleteCommentReq{
		Id:    d.Id,
		BizId: d.BizId,
	}
}

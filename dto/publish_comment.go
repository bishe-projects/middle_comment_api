package dto

import "gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/comment"

type PublishComment struct {
	BizId        int64  `json:"biz_id" binding:"required"`
	EntityTypeId int64  `json:"entity_type_id" binding:"required"`
	EntityId     int64  `json:"entity_id" binding:"required"`
	Uid          int64  `json:"uid" binding:"required"`
	Content      string `json:"content" binding:"required"`
	ParentId     int64  `json:"parent_id" binding:"required"`
}

func (d *PublishComment) ConvertToReq() *comment.PublishCommentReq {
	return &comment.PublishCommentReq{
		BizId:        d.BizId,
		EntityTypeId: d.EntityTypeId,
		EntityId:     d.EntityId,
		Uid:          d.Uid,
		Content:      d.Content,
		ParentId:     d.ParentId,
	}
}

package dto

import "gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/comment"

type UserCommentList struct {
	BizId        int64  `json:"biz_id" binding:"required"`
	EntityTypeId int64  `json:"entity_type_id" binding:"required"`
	Uid          int64  `json:"uid" binding:"required"`
	PageNum      *int64 `json:"page_num,omitempty"`
	PageSize     *int64 `json:"page_size,omitempty"`
}

func (d *UserCommentList) ConvertToReq() *comment.UserCommentListReq {
	return &comment.UserCommentListReq{
		BizId:        d.BizId,
		EntityTypeId: d.EntityTypeId,
		Uid:          d.Uid,
		PageNum:      d.PageNum,
		PageSize:     d.PageSize,
	}
}

package dto

import "gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/comment"

type CreateCommentEntity struct {
	BizId int64  `json:"biz_id" binding:"required"`
	Name  string `json:"name" binding:"required"`
	Desc  string `json:"desc" binding:"required"`
}

func (d *CreateCommentEntity) ConvertToReq() *comment.CreateCommentEntityReq {
	return &comment.CreateCommentEntityReq{
		BizId: d.BizId,
		Name:  d.Name,
		Desc:  d.Desc,
	}
}
